package day12.morn;

import java.sql.Date;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Locale;

public class InternationalDemo2 {
	public static void main(String[] args) {
		Locale l =Locale.ENGLISH;
		NumberFormat informatter = NumberFormat.getCurrencyInstance(new Locale("en", "IN"));
		
		Integer i = 1_5_0_0_0_974;
		NumberFormat formatter = NumberFormat.getNumberInstance(l);
		System.out.println(formatter.format(i));
		System.out.println(String.format("Locale: %s; int: %s",l, formatter.format(i)));
		
		NumberFormat formatter2 = NumberFormat.getCurrencyInstance(l);
		System.out.println(String.format("Locale: %s; currency: %s",l, informatter.format(i)));
		
		SimpleDateFormat formatter3 = (SimpleDateFormat) SimpleDateFormat.getDateInstance(DateFormat.DEFAULT,l);
//		Date d = new Date(i);
		LocalDate d = LocalDate.now();
		System.out.println(String.format("Locale: %s, Date: %s",l,formatter3.format(d)));
		
		String pattern= "EEE, MMM d, yyyy";
		SimpleDateFormat sdf= new SimpleDateFormat(pattern,l);
		System.out.println(String.format("Locale: %s; custom date format: %s",l, sdf.format(d)));
	}
}
