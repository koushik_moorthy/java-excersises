package day12.morn;

import java.io.FileInputStream;
import java.util.Enumeration;
import java.util.Properties;

public class PropDemo {
	public static void main(String[] args)throws Exception {
		Properties p = new Properties();
		p.load(new FileInputStream("abcdef.properties"));
		System.out.println(p.get("hello"));
		
		Enumeration e= p.elements();
		while(e.hasMoreElements()) {
			System.out.println(e.nextElement());
		}
	}
}
