package day12.morn;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;

public class FIleIODemo {
	public static void main(String[] args) throws Exception {
//		new ReadByte().read();
//		new ReadChar().read();
		new WriteChar().write();
	}
}

class ReadByte{
	public void read() throws Exception {
		FileInputStream fs = new FileInputStream("abc.txt");
		System.out.println(fs.available());
		byte b[] = new byte[6];
		int i=0;
		while((i=fs.read(b))!=-1) {
			String s = new String(b,0,i);
			System.out.println(s);
		}
	}
}

class WriteByte{
	public void write() throws Exception{
		FileInputStream fis = new FileInputStream("abc.txt");
		FileOutputStream fos = new FileOutputStream("abc1.txt");
		byte b[] = new byte[6];
		int i=0;
		while((i=fis.read(b))!=-1) {
			fos.write(b,0,i);
		}
	}
}

class ReadChar{
	public void read() throws Exception{
		FileReader fr = new FileReader("abc.txt");
		char c[] = new char[4];
		int i =0;
		while((i=fr.read(c))!=-1) {
			String s = new String(c,0,i);
			System.out.println(s);
		}
	}
}


class WriteChar{
	public void write() throws Exception{
		FileReader fr = new FileReader("abc.txt");
		FileWriter fw = new FileWriter("abc1.txt");
		fw.write("Hello");
		char c[] = new char[4];
		int i=0;
		while((i=fr.read(c))!=-1) {
			fw.write(c,0,i);
		}
		fw.close();
	}
}



