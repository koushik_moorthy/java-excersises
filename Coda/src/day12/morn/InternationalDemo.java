package day12.morn;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.util.Locale;
import java.util.ResourceBundle;

public class InternationalDemo {
	public static void main(String[] args) throws Exception{
		Locale locale = Locale.getDefault();
		System.out.println(locale);
		locale = new Locale("te");
		Locale.setDefault(locale);
		System.out.println(Locale.getDefault());
		
		String head = "<meta http-equiv=Content-Type content=text/html; charset=utf-8>";
		ResourceBundle rb = ResourceBundle.getBundle("day12.morn.Dictionary",Locale.getDefault());
		String s = rb.getString("hello");
		
		FileOutputStream fos = new FileOutputStream("abc.html", true);
//		fos.write(s.getBytes());
		BufferedWriter out = new BufferedWriter(new OutputStreamWriter(fos,Charset.forName("UTF-8")));
		out.write(head);
//		out.write("Hello World");
		out.write(s);
		out.close();
	}
}
