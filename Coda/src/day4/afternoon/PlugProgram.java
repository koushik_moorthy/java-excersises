package day4.afternoon;

public class PlugProgram {
	public static void main(String[] args) {
		Ssocket ss=new Ssocket();
		Lplug lp=new Lplug();
		IndianAdapter ia=new IndianAdapter();
		ia.ap=lp;
		ss.roundPinhole(ia);
	}
}
abstract class IndianPlug{
	public abstract void roundPin();
}
class Splug extends IndianPlug{
	@Override
	public void roundPin(){
		System.out.println("Indian pin");
	}
}
abstract class Isocket{
	public abstract void roundPinhole(IndianPlug ip);
}
class Ssocket extends Isocket{
	@Override
	public void roundPinhole(IndianPlug ip){
		ip.roundPin();
	}
}
abstract class Aplug{
	public abstract void slabPin();
}
class Lplug extends Aplug{
	@Override
	public void slabPin(){
		System.out.println("American  slab pin working");
	}
}
class IndianAdapter extends IndianPlug{
	Aplug ap;
	@Override
	public void roundPin(){
		ap.slabPin();
	}
}