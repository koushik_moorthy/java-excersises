package day4.morning;

import java.util.Scanner;

public class GoodPgm {
	public static void main(String[] args) throws Exception{
		Dog tiger = new Dog();
		Child baby=new Child();
		Scanner scan= new Scanner(System.in);
		System.out.println("Enter an item name");
		String itemclassname=scan.next();
		Item item = (Item)Class.forName("day4.morning."+itemclassname).newInstance();
		baby.playWithDog(tiger,item);
	}
}
class Child{
	public void playWithDog(Dog dog,Item item) {
		dog.play(item);
	}
}

class Dog{
	public void play(Item item) {
		item.execute();
	}
}
abstract class Item{
	public abstract void execute();
}
class Stick extends Item{
	@Override
	public void execute() {
		// TODO Auto-generated method stub
		System.out.println("You beat I bite"); 
	}
}

class Stone extends Item{
	@Override
	public void execute() {
		// TODO Auto-generated method stub
		System.out.println("You hit I bite"); 
	}
}