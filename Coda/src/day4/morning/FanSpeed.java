package day4.morning;

import java.util.Scanner;

public class FanSpeed {
	public static void main(String[] args) throws Exception{
		GoodFan gd=new GoodFan();
		Scanner sc=new Scanner(System.in);
		while(true){
			System.out.println("Enter to pull");
			sc.next();
			gd.pull();
		}
	}
}

class GoodFan{
	State state = new SwitchOffState();
	public void pull() {
		state.execute(this);
	}
}

abstract class State{
	public abstract void execute(GoodFan f);
}



class SwitchOffState extends State{
	@Override
	public void execute(GoodFan f) {
		System.out.println("Switched On");
		f.state = new MediumSpinState();
	}
}

class MediumSpinState extends State{
	@Override
	public void execute(GoodFan f) {
		System.out.println("Medium Spin");
		f.state = new HighSpinState();
	}
}

class HighSpinState extends State{
	@Override
	public void execute(GoodFan f) {
		System.out.println("High Spin");
		f.state = new SwitchIsOff();
	}
}

class SwitchIsOff extends State{
	@Override
	public void execute(GoodFan f) {
		System.out.println("Switched Off");
		f.state = new SwitchOffState();
	}
}