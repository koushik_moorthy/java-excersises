package day4.morning;

import java.lang.reflect.Constructor;

public class ClassForName {
	public static void main(String[] args) throws Exception {
		Constructor t = Class.forName("day4.morning.Test1").getConstructor(String.class);
		Test t1= (Test)t.newInstance("Hello");
		
	}
}
abstract class Test{public abstract void work();}

class Test1 extends Test{
	public Test1(String name) {
		System.out.println("Test1 constructor method called"+name);
	}
	@Override
	public void work() {
		System.out.println("Test1 work method called");
	}
}


class Test2 extends Test{
	@Override
	public void work() {
		System.out.println("Test2 work method called");
	}
}
