package project.chatapp;

import java.io.*;
import java.net.*;

public class UsersInfo extends Thread {
    private Socket socket;
    private Server server;
    private PrintWriter writer;
 
    public UsersInfo(Socket socket, Server server) {
        this.socket = socket;
        this.server = server;
    }
 
    public void run() {
        try {
            InputStream input = socket.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(input));
 
            OutputStream output = socket.getOutputStream();
            writer = new PrintWriter(output, true);
 
            printUsers();
 
            String userName = reader.readLine();
            server.addUserName(userName);

            String serverMessage = "*** New user connected: " + userName+" ***";
            server.broadcast(serverMessage, this);
//            server.sendUserName(userName);
            String clientMessage;
            
            do {
            	
                clientMessage = reader.readLine();
                serverMessage = "[" + userName + "]: " + clientMessage;
                server.broadcast(serverMessage, this);
 
            } while (!clientMessage.equals("quit"));
 
            server.removeUser(userName, this);
            socket.close();
 
            serverMessage = "*** "+userName + " has left the conversation ***";
            server.broadcast(serverMessage, this);
 
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
 
    void printUsers() {
        if (server.hasUsers()) {
            writer.println("*** Connected users: " + server.getUserNames()+" ***");
        } else {
            writer.println("No other users connected");
        }
    }
 
    void sendMessage(String message) {
        writer.println(message);
    }
}