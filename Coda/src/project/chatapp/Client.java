package project.chatapp;

import java.net.*;

public class Client {
    private String hostname;
    private int port;
    private String userName;
 
    public Client(String hostname, int port) {
        this.hostname = hostname;
        this.port = port;
    }
 
    public void execute() {
        try {
            Socket socket = new Socket(hostname, port);
 
            System.out.println("Connected to the chat server");
 
            new ReadMsgs(socket, this).start();
            new WriteMsgs(socket, this).start();
 
        } catch (Exception ex) {
        	ex.printStackTrace();
        }
 
    }
 
    void setUserName(String userName) {
        this.userName = userName;
    }
 
    String getUserName() {
        return this.userName;
    }
 
 
    public static void main(String[] args) {
 
        String hostname = "localhost";
        int port = Integer.parseInt(args[0]);
 
        Client client = new Client(hostname, port);
        client.execute();
    }
}