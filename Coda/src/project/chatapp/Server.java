package project.chatapp;

import java.net.*;
import java.util.*;

public class Server {
    private int port;
    private Set<String> userNames = new HashSet<>();
    private Set<UsersInfo> userThreads = new HashSet<>();
 
    public Server(int port) {
        this.port = port;
    }
 
    public void execute() {
        try (ServerSocket serverSocket = new ServerSocket(port)) {
 
            System.out.println("Chat Server is listening on port " + port);
 
            while (true) {
                Socket socket = serverSocket.accept();
                System.out.println("New user connected");
 
                UsersInfo newUser = new UsersInfo(socket, this);
                userThreads.add(newUser);
                newUser.start();
            }
 
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
 
    public static void main(String[] args) {
        int port = Integer.parseInt(args[0]);
        Server server = new Server(port);
        server.execute();
    }
 
    void broadcast(String message, UsersInfo excludeUser) {
        for (UsersInfo aUser : userThreads) {
            if (aUser != excludeUser) {
                aUser.sendMessage(message);
            }
        }
    }
 
    void addUserName(String userName) {
        userNames.add(userName);
        System.out.println(userName+" joined");
    }
 
    void removeUser(String userName, UsersInfo aUser) {
        boolean removed = userNames.remove(userName);
        if (removed) {
            userThreads.remove(aUser);
            System.out.println("The user " + userName + " has left the conversation");
        }
    }
 
    Set<String> getUserNames() {
        return this.userNames;
    }
 
    boolean hasUsers() {
        return !this.userNames.isEmpty();
    }
}