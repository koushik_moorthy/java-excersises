package project.chatapp;

import java.io.*;
import java.net.*;
 
public class ReadMsgs extends Thread {
    private BufferedReader reader;
    private Client client;
    public ReadMsgs(Socket socket, Client client) {
        this.client = client;
        try {
            InputStream input = socket.getInputStream();
            reader = new BufferedReader(new InputStreamReader(input));
        } catch (IOException ex) {
            System.out.println("Error getting input stream: " + ex.getMessage());
            ex.printStackTrace();
        }
    }
 
    public void run() {
        while (true) {
            try {
                String response = reader.readLine();
                System.out.println("\n" + response);
 
                // prints the username after displaying the server's message
                if (client.getUserName() != null) {
                    System.out.print("[" + client.getUserName() + "]: ");
                }
            } catch (Exception ex) {
                ex.printStackTrace();
                break;
            }
        }
    }
}