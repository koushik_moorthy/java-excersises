package project.chatapp;

import java.io.*;
import java.net.*;
 
public class WriteMsgs extends Thread {
    private PrintWriter writer;
    private Socket socket;
    private Client client;
    private BufferedReader in;
    private String userName;
    private String text;
    
    public WriteMsgs(Socket socket, Client client) {
        this.socket = socket;
        this.client = client;
 
        try {
            OutputStream output = socket.getOutputStream();
            writer = new PrintWriter(output, true);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
 
    public void run(){ 
    	
        try {
        System.out.print("Enter username......");
        
        writer = new PrintWriter(socket.getOutputStream(),true);
        in = new BufferedReader(new InputStreamReader(System.in));
		userName = in.readLine();
		writer.println(userName);
        client.setUserName(userName);
        }
        catch(Exception e) {
        	e.printStackTrace();
        }
 
        do {
        	try {
        	System.out.print("[" + userName + "]: ");
            text = in.readLine();
    		writer.println(text);
        	}
        	catch(Exception e) {
        		e.printStackTrace();
        	}
        } while (!text.equals("quit"));
 
        try {
            socket.close();
        } catch (Exception ex) {
        	ex.printStackTrace();
        }
    }
}