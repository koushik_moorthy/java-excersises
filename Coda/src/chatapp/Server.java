package chatapp;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Server {
	ServerSocket ss;
	Socket s;
	BufferedReader in;
	PrintWriter out;
	
	public Server() {
		try {
			ss= new ServerSocket(2230);
			System.out.println("Ready to accept....");
			while(true) {
				s=ss.accept();
				ExecutorService es= Executors.newFixedThreadPool(4);
				es.execute(()->{
					try {
						while(true) {
								in = new BufferedReader(new InputStreamReader(s.getInputStream()));
								String msgfromclient = in.readLine();
								System.out.println("Message from Client...."+msgfromclient);
								
								System.out.println("Enter message for client....");
								in = new BufferedReader(new InputStreamReader(System.in));
								String msg= in.readLine();
								
								out=new PrintWriter(s.getOutputStream(),true);
								out.println(msg);
								}
					}
					catch (Exception e) {
						// TODO: handle exception
						e.printStackTrace();
					}
				});
				es.shutdown();
			}
			
		}
		catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}
	}
	public static void main(String[] args) {
		new Server();
	}
}