package chatapp;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class Client {
	Socket s; 
	BufferedReader in;
	PrintWriter out;
	
	public Client() {
		try {
			while(true) {
				s = new Socket("localhost",2230);
				
				System.out.println("Enter message");
				in = new BufferedReader(new InputStreamReader(System.in));
				String msg = in.readLine();
				out = new PrintWriter(s.getOutputStream(),true);
				out.println(msg);
				
				in= new BufferedReader(new InputStreamReader(s.getInputStream()));
				String msgfromserver = in.readLine();
				System.out.println("Message is ..."+msgfromserver);
			}
		}
		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
	public static void main(String[] args) {
		new Client();
	}
	
}
