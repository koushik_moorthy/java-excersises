package streams;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.OptionalDouble;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.LongStream;
import java.util.stream.Stream;

public class StreamDemo {
	public static void main(String[] args) {
		StreamDemo str = new StreamDemo();
//		str.streamConcat();
//		str.streamArray();
//		str.parallelStream();
//		str.collectorArray();
//		str.intStreamLongStream();
//		str.limitGenerate();
//		str.mapFunctionality();
		str.skipreduceFunctionality();
//		str.sortingDemo();
//		str.minmaxFun();
//		str.peekFun();
	}
	
	public void streamConcat() {
		Stream<String> strm= Stream.of("aaa","bbb","ccc");
		Stream<String> strm1 = Stream.of("Dubai","Main","Road");
		Stream.concat(strm, strm1).forEach(System.out::println);;
	}
	
	public void	streamArray(){
		String[] arr = {"111","222","333"};
		Stream<String> strm = Arrays.stream(arr);
		strm.forEach(System.out::println);
	}
	public void parallelStream() {
		Stream<String> strm = Stream.of("hello","dubai","Ah");
		strm.parallel().forEach(System.out::println);
	}
	
	public void collectorArray() {
		//performing stream operation on lists
		List<Integer> ls = new ArrayList<>();
		ls.add(2);
		ls.add(22);
		ls.add(223);
		ls = ls.stream().map(n->n+2).collect(Collectors.toList());
		System.out.println(ls);
	}
	
	public void intStreamLongStream() {
		//anymatch,allmatch,average
		//IntStream
		int arr[] = {12,231,321,423,123,4321};
		int key=12;
		boolean result = IntStream.of(arr).anyMatch(n->(n==key));
		if(result) {
			System.out.println("Element found");
		}
		else {
			System.out.println("Element not found");
		}
		
		//LongStream
		long arr1[] = {12,10,9,11,8};
		int key1=4321;
		int i=0;
		Double result1 = LongStream.of(arr1).average().getAsDouble();
		System.out.println(result1);
	}
	
	public void limitGenerate() {
		//distinct,limit
		Stream strm = Stream.of(3,2,2,1,3,1).distinct().limit(3);
		strm.forEach(i->System.out.println(i));
		
		Stream<Double> str = Stream.<Double>generate(()->new RandomGen().generatedMeth()).limit(5);
		str.forEach(System.out::println);
	}
	
	public void mapFunctionality() {
		//passmarks
		Stream strm = Stream.of(60,70,10,40,90).filter(n->n>35).map(n->n+2);
		strm.forEach((i)->System.out.println(i));
		
	}
	public void skipreduceFunctionality() {
		Stream str =Stream.of(1,2,3,4,5,6,7).skip(3);
		str.forEach(System.out::println);
		int val =Stream.of(1,2,3,4,5,6,7).reduce((n1,n2)->n1+n2).get();
		System.out.println(val);
	}
	
	public void sortingDemo() {
//		Stream.of(1,5,2,3,4,6,7,1,2,314,3,54).distinct().sorted().forEach(System.out::println);;
//		Stream.of(1,5,12,43,5465,6523,312,42,5,3).distinct().sorted((i1,i2)->-i1.compareTo(i2)).forEach(System.out::println);
//		Stream.of(21,324,234,1,1223,234,324,243,234,12).distinct().sorted((o1,o2)->new MyComparator().compare(o1, o2)).forEach(System.out::println);;
		
		
		List<Integer> ls =new ArrayList<Integer>();
		ls.add(1);
		ls.add(12);
		ls.add(1123);
		ls.add(123);
		ls.add(1123);
		ls.add(423);
		ls.add(123);
		
		 ls = ls.stream().distinct().sorted((i1,i2)->i1.compareTo(i2)).collect(Collectors.toList());
		 System.out.println(ls);
	}
	
	public void minmaxFun() {
		Stream<Integer> str= Stream.of(12,321,231,213,432,543546,64,3455,435,2423);
		int min = str.min((i1,i2)->i1.compareTo(i2)).get();
		System.out.println(min);
		
		Stream<Integer>  strm1 = Stream.of(123,231,43,12,34,14,413,2,432,234).distinct();
		int max = strm1.max((i1,i2)->i1.compareTo(i2)).get();
		System.out.println(max);
		Stream<Integer>  strm2 = Stream.of(123,231,43,12,34,14,413,2,432).distinct();
		long count = strm2.count();
		System.out.println(count);
	}
	public void peekFun() {
		Stream.of(12,21,422,3,545,423).peek(System.out::println).count();
	}
}

class RandomGen{
	public double generatedMeth() {
		return new Random().nextDouble();
	}
}

class MyComparator implements Comparator{
	@Override
	public int compare(Object o1, Object o2) {
		// TODO Auto-generated method stub
		Integer i1 = (Integer)o1;
		Integer i2 = (Integer)o2;
		return i1.compareTo(i2);
	}
	
}