	package day7.morn;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class FieldMethodClassDemo {
	public static void main(String[] args) throws Exception {
		PoliceStation p= new PoliceStation();
		Politician ramu = new Politician();
		Naxalite nax = new Naxalite();
		p.arrest(ramu);
	}
}
class PoliceStation{
	public void arrest(Object p) throws Exception{
		Class c = p.getClass();
		Field field = c.getField("name");
		System.out.println(field.get(p));
//		Field fields[] = c.getFields();
//		for (Field f:fields) {
//			System.out.println(f.getName());
//		}
		Method met = c.getMethod("work");
		met.invoke(p);
//		Method methods[] = c.getMethods();
//		for(Method m:methods) {
//			System.out.println(m.getName());
//		}
		
		tortureroom(p);
	}
	public void tortureroom(Object p)throws Exception {
		Class c= p.getClass();
		Field field = c.getDeclaredField("secretName");
		field.setAccessible(true);
		System.out.println(field.get(p));
		
		Method met = c.getDeclaredMethod("doSecretWork");
		met.setAccessible(true);
		met.invoke(p);
		
		
	}
}

class Politician{
	public String name="Arasiyal Vaadhi";
	public String getName(String s) {
		s = this.name;
		return s;
	}
	private String secretName = "Katta Panjayathu King";
	public void work() {
		System.out.println("Do Katta Panchayat");
	}
	public void doSecretWork() {
		System.out.println("Do Bribery among people");
	}
}

class Naxalite{
	public String name = "Laden";
	public void work() {
		System.out.println("I am a Terrorist");
	}
}
