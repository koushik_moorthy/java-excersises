package day7.morn;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

public class InterfaceSubjectionDemo {
	public static void main(String[] args) {
		AlopathyMedicalCollege aloe= new AlopathyMedicalCollege();
		AyurvedaMedicalCollege ayur = new AyurvedaMedicalCollege();
		JetAcademy jet = new JetAcademy();
		
		Human shoaib = new Human();
		
		
		
		Object humanshoaib = Proxy.newProxyInstance(Human.class.getClassLoader(), new Class[] {Doctor.class,Pilot.class},new MyInvocationHandler(new Object[] {aloe,jet})); 
		
		
		Pilot pilot = (Pilot)humanshoaib;
		pilot.fly();
	}
}

class MyInvocationHandler implements InvocationHandler{
	Object o;
	Object obj[];
	
	public MyInvocationHandler(Object obj[]) {
		// TODO Auto-generated constructor stub
		this.obj = obj;
	}
	@Override
	public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
		// TODO Auto-generated method stub
		
			for(int i=0;i<obj.length;i++) {
				try {
				o = method.invoke(obj[i], args);
				}
				catch(Exception e) {
					
				}
		
		}
		
		return o;
	}
}

interface Doctor{
	public void doCure();
}

interface Nurse{
	public void takeCare();
}

class AlopathyMedicalCollege implements Doctor,Nurse{
	@Override
	public void doCure() {
		// TODO Auto-generated method stub
		System.out.println("Doing Curing");
	}
	@Override
	public void takeCare() {
		// TODO Auto-generated method stub
		System.out.println("Doing Nursing");
	}
}

class AyurvedaMedicalCollege implements Doctor,Nurse{
	@Override
	public void doCure() {
		// TODO Auto-generated method stub
		System.out.println("Doing Curing in  Ayurveda");
	}
	@Override
	public void takeCare() {
		// TODO Auto-generated method stub
		System.out.println("Doing Nursing in Ayurveda");
	}
}

interface Pilot{
	public void fly();
}

interface Steward{
	public void serve();
}

class JetAcademy implements Pilot,Steward{
	@Override
	public void fly() {
		// TODO Auto-generated method stub
		System.out.println("Am flying");
	}
	public void serve() {
		System.out.println("Am Serving");
	};
}

class Human{
	
}