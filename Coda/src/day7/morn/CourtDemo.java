package day7.morn;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class CourtDemo {
	 public static void main(String[] args) throws Exception {
		Court crt = new Court();
		Thief balram = new Thief();
		Police police = new Police();
		System.out.println("Lets hear from the Thief");
		crt.getInfo(balram);
		System.out.println("Lets hear from the Police");
		crt.getInfo(police);
	}
}


class Court{
	public void getInfo(Object obj) throws Exception {
		Class cls = obj.getClass();
		Field field = cls.getField("name");
		System.out.println("I am "+field.get(obj));
		
		Method meth = cls.getMethod("narrateIncident");
		meth.invoke(obj);
		knockHim(obj);
	}
	
	public void knockHim(Object obje)throws Exception {
		try {
		Class c= obje.getClass();
		Field field = c.getDeclaredField("secretName");
		field.setAccessible(true);
		System.out.println(field.get(obje));
		
		Method met = c.getDeclaredMethod("narrateOriginalIncident");
		met.setAccessible(true);
		met.invoke(obje);
		}
		catch(Exception e) {}
	}

}

class Police{

	public String name="Anbu Selvan IAS";
	public void narrateIncident() {
		System.out.println("Thief was caught right handed when he was stealing...");
	}
			
}

class Thief{
	public String name="Balram";
	public void narrateIncident() {
		System.out.println("I didnt do that I was just standing there...");
	}
	
	private String secretName = "The Don";
	public void narrateOriginalIncident() {
		System.out.println("I did that I feel guilty....");
	}
}


