package day7.noon;

public class CreamCompos {
	public static void main(String[] args) {
		IceCream cream = new Nuts();
		System.out.println(cream.cost());
	}
}

abstract class IceCream{
	public abstract int cost();
}

abstract class Cream extends IceCream{
	
}

class Vanilla extends Cream{
	IceCream cream;
	public Vanilla() {
		// TODO Auto-generated constructor stub
	}
	public Vanilla(IceCream cream) {
		// TODO Auto-generated constructor stub
		this.cream = cream;
	}
	
	@Override
	public int cost() {
		// TODO Auto-generated method stub
		if(cream!=null) {
			return 5+cream.cost();
		}
		else {
			return 5;
		}
	}
}

class Strawberry extends Cream{
	IceCream cream;
	public Strawberry() {
		// TODO Auto-generated constructor stub
	}
	public Strawberry(IceCream cream) {
		// TODO Auto-generated constructor stub
		this.cream = cream;
	}
	@Override
	public int cost() {
		// TODO Auto-generated method stub
		if(cream!=null) {
			return 5+cream.cost();
		}
		else {
			return 5;
		}
	}
}

class Chocolate extends Cream{
	IceCream cream;
	public Chocolate() {
		// TODO Auto-generated constructor stub
	}
	public Chocolate(IceCream cream) {
		// TODO Auto-generated constructor stub
		this.cream = cream;
	}
	@Override
	public int cost() {
		// TODO Auto-generated method stub
		if(cream!=null) {
			return 5+cream.cost();
		}
		else {
			return 5;
		}
	}
}

abstract class Ingredients extends IceCream{}

class Nuts extends Ingredients{
	IceCream cream;
	public Nuts() {
		// TODO Auto-generated constructor stub
	}
	public Nuts(IceCream cream) {
		// TODO Auto-generated constructor stub
		this.cream = cream;
	}
	@Override
	public int cost() {
		// TODO Auto-generated method stub
		if(cream!=null) {
			return 5+cream.cost();
		}
		else {
			return 5;
		}
	}
}

class Fruits extends Ingredients{
	IceCream cream;
	public Fruits() {
		// TODO Auto-generated constructor stub
	}
	public Fruits(IceCream cream) {
		// TODO Auto-generated constructor stub
		this.cream = cream;
	}
	@Override
	public int cost() {
		// TODO Auto-generated method stub
		if(cream!=null) {
			return 5+cream.cost();
		}
		else {
			return 5;
		}
	}
}