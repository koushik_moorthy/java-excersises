package day7.noon;

import java.lang.ref.SoftReference;
//import java.lang.ref.WeakReference;

public class GCDemo {
	public static void main(String[] args) {
		Runtime r = Runtime.getRuntime();
		System.out.println("Before Grandpa's birth...."+r.freeMemory());
		GrandPa gr = new GrandPa();
		System.out.println("After Grandpa's birth...."+r.freeMemory());
		
		SoftReference<GrandPa> soft = new SoftReference<GrandPa>(gr);
//		WeakReference<GrandPa> weak = new WeakReference<GrandPa>(gr);
		gr = null;
		System.out.println("After Grandpa's Death...."+r.freeMemory());
		r.gc();
		System.out.println("After Grandpa's Death Funeral...."+r.freeMemory());
//		gr= weak.get();
//		System.out.println("Grandpa age ...."+gr.age);
		
	}
}

class GrandPa{
	String age;
	private String goldLocation = "Its in the treasury...";
	public GrandPa() {
		// TODO Auto-generated constructor stub
		for(int i=0;i<10000;i++) {
			age = new String(i+"");
		}
	}
	
	private void locationofGold(){
		System.out.println("GrandPa's notebook caught:  Gold Location: "+goldLocation);
	}
	
	protected void finalize() throws Throwable{
		System.out.println("Finalize method called..");
	}
}