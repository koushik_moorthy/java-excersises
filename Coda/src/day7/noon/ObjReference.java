package day7.noon;

public class ObjReference {
	public static void main(String[] args) {
			System.out.println("First Scenario - two objects created...");
			Sheep sheep1 = new Sheep();
			Sheep sheep2 = new Sheep();
			sheep1.name = "Mother";
			sheep2.name = "Duplicate";
			System.out.println(sheep1.name+"......."+sheep2.name);
			
			//Second Scenario
			
			System.out.println("second scene one object with two references created...");
			Sheep s1 = new Sheep();
			Sheep s2 = s1;
			
			s1.name= "Mother Mother....";
			s2.name="dup dup dupe....";
			
			System.out.println(s1.name+"............."+s2.name);
			
			
			
			
			System.out.println("Third Scenario");
			Sheep mother = new Sheep();
			Sheep dolly = mother.createProto();
			mother.name="I am mother";
			dolly.name="I am dolly the clone";
			System.out.println(mother.name+"..........."+dolly.name);
			
	}
}

class Sheep implements Cloneable{
	String name;
	public Sheep() {
		System.out.println("Sheep Cons Called");
	}
	public Sheep createProto() {
		try {
			return (Sheep)super.clone();
		}
		catch(Exception e) {
			e.printStackTrace();
			return null;
		}
		
	}
}
