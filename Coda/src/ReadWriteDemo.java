import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.PrintWriter;

public class ReadWriteDemo {
	public static void main(String[] args) throws Exception {
//		new Reader().read();
		new Writer().write();
	}
}

class Reader{
	public void read() throws Exception {
		BufferedReader br = new BufferedReader(new FileReader("abc.txt"));
		String line;
		while((line=br.readLine())!=null) {
			System.out.println(line);
		}
		br.close();
	}
	
}

class Writer{
	public void write() throws Exception {
		PrintWriter out = new PrintWriter("copy.txt");
		BufferedReader br = new BufferedReader(new FileReader("abc.txt"));
		String line;
		while((line=br.readLine())!=null) {
			out.write(line);
		}
		out.flush();
	}
}