package revision_day5;

public class UniversalRemote {
	Commands c[]=new Commands[5];
	public UniversalRemote() {
		for(int i=0;i<c.length;i++) {
			c[i]=new DummyCommands();
		}
	}
	
	public void setCommands(int slot,Commands Commands) {
		c[slot]=Commands;
	}
	
	public void executeCommands(int slot) {
		c[slot].execute();
	}
	
}
abstract class Commands{
	Tv tv;SetTopBox st;NetFlix nf;SoundSystem ss;AmazonPrime ap;
	public Commands() {
		init();
	}
	void init() {
		tv=new Tv();st=new SetTopBox();nf=new NetFlix();ss=new SoundSystem();ap=new AmazonPrime();
	}
	public abstract void execute();
}
class DummyCommands extends Commands{
	@Override
	public void execute() {
		System.out.println("I am dummy yet to be operational....");
	}
}
class SerialChannelCommands extends Commands{
	@Override
	public void execute() {
		//process done by the worker - being automated...
		System.out.println("serial channel work started.....");
		tv.switchOn();
		tv.av1Mode();
		st.serialChannel();
		ss.lowSound();
		System.out.println("enjoy the serial....");
	}
}
class NewsChannelCommands extends Commands{
@Override
	public void execute() {
	System.out.println("news channel work started.....");
	tv.switchOn();
	tv.av1Mode();
	st.newsChannel();
	ss.highSound();
	System.out.println("enjoy the news....");
		
	}	
}