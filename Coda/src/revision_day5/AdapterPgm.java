package revision_day5;

public class AdapterPgm {
	public static void main(String[] args) {
		ShakthiSocket ss= new ShakthiSocket();
		HPPlug hp =new HPPlug();
		ShakthiAdapter sa=new ShakthiAdapter();
		sa.ap=hp;
		ss.roundPinHole(sa);
	}
}

abstract class AmericanPlug{
	public abstract void slabPin();
}
class HPPlug extends AmericanPlug{
	@Override
	public void slabPin() {
		// TODO Auto-generated method stub
		System.out.println("Slab Pin Working");
	}
}
abstract class IndianSocket{
	public abstract void roundPinHole(IndianAdapter ia);
}
class ShakthiSocket extends IndianSocket{
	@Override
	public void roundPinHole(IndianAdapter ia) {
		// TODO Auto-generated method stub
		ia.roundPin();
	}
}
abstract class IndianAdapter{
	public abstract void roundPin();
}
class ShakthiAdapter extends IndianAdapter{
	AmericanPlug ap;
	@Override
	public void roundPin() {
		// TODO Auto-generated method stub
		ap.slabPin();
	}
}