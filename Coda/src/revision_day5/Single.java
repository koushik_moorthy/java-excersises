package revision_day5;

public class Single {
	public static void main(String[] args) {
		Singleton s = Singleton.createSingleton();
		Singleton s1 = Singleton.createSingleton();
		Singleton s2 = Singleton.createSingleton();
	}
}

class Singleton{
	private Singleton(){
		System.out.println("Singleton cons called");
	}
	static Singleton s;
	public static Singleton createSingleton() {
		if(s==null) {
			s= new Singleton();
			return s;
		}
		else {
			return s;
		}
	}
	
}