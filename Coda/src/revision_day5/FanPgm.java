package revision_day5;

import java.util.Scanner;

public class FanPgm {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		GoodFan f =new GoodFan();
		while(true) {
			System.out.println("Enter key to pull");
			scan.next();
			f.execute();
		}
		
	}
}
class GoodFan{
	State state = new SwitchOffState();
	public void execute() {
		state.execute(this);
	}
}

abstract class State{
	public abstract void execute(GoodFan f);
}

class SwitchOffState extends State{
	@Override
	public void execute(GoodFan f) {
		// TODO Auto-generated method stub
		System.out.println("Switch on");
		f.state = new MediumSpinState();
	}
}
class MediumSpinState extends State{
	@Override
	public void execute(GoodFan f) {
		// TODO Auto-generated method stub
		System.out.println("MediumSpinState");
		f.state = new SwitchOffState();
	}
}