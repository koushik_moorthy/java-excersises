package revision_day5;

public class SingleTonPgm {
	public static void main(String[] args) {
		SingleTonEx s = SingleTonEx.createSingleTon();
		SingleTonEx s2 = SingleTonEx.createSingleTon();
		SingleTonEx s3 = SingleTonEx.createSingleTon();
		
		MultiTon m= new MultiTon();
		MultiTon m1= new MultiTon();
	}
}

class SingleTonEx{
	private SingleTonEx() {
		System.out.println("Singleton object Created");
	}
	static SingleTonEx s;
	public static SingleTonEx createSingleTon() {
		if(s==null) {
			s = new SingleTonEx();
			return s;
		}
		else {
			return s;
		}
	}
}

class MultiTon {
	public MultiTon() {
		System.out.println("Multi Ton object Created");
	}
}