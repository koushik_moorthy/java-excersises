package revision_day5;

public class DogPgm {
	public static void main(String[] args) {
		Child1 c= new Child1();
		Dog1 d = new Dog1();
		Item1 i = new Stone1();
		c.play(d,i);
	}
}

class Child1{
	public void play(Dog1 d,Item1 i) {
		d.playwithItem(i);
	}
}

class Dog1{
	public void playwithItem(Item1 i) {
		System.out.println(i);
	}
}

abstract class Item1{}

class Stick1 extends Item1{}
class Stone1 extends Item1{}