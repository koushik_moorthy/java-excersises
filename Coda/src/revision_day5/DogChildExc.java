package revision_day5;

public class DogChildExc {
	public static void main(String[] args) {
		Child baby = new Child();
		Dog tiger = new Dog();
		Item item = new Stone();
		baby.playwithDog(tiger, item);
	}
}

class Child{
	public void playwithDog(Dog dog,Item item) {
		try {
		dog.playwithItem(item);
		}
		catch(DogException de) {
			de.visit();
		}
	}
}

class Handle extends DogException{
	public void handle(DogBarkException dbe) {
		System.out.println(dbe+" and it calmed down");
	}
	public void handle(DogBiteException dbie) {
		System.out.println(dbie+" harshly take me to doc");
	}
	@Override
	public void visit() {
		// TODO Auto-generated method stub
		
	}
}

class Dog{
	public void playwithItem(Item item) throws DogException {
		item.execute();
	}
}

abstract class Item{
	public abstract void execute() throws DogException;
}

class Stick extends Item{
	@Override
	public void execute() throws DogException{
		// TODO Auto-generated method stub
		throw new DogBarkException();
	}
}

class Stone extends Item{
	@Override
	public void execute() throws DogException{
		// TODO Auto-generated method stub
		throw new DogBiteException();
	}
}

abstract class DogException extends Exception{
	public abstract void visit();
}

class DogBarkException extends DogException{
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "Dog Barked at me";
	}
	@Override
	public void visit() {
		// TODO Auto-generated method stub
		new Handle().handle(this);
	}
}

class DogBiteException extends DogException{
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "Dog bited me";
	}
	@Override
	public void visit() {
		// TODO Auto-generated method stub
		new Handle().handle(this);
	}
}