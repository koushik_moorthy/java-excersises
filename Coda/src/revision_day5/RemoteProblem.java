package revision_day5;

public class RemoteProblem {
	public static void main(String[] args) {
		NewsChannelCommands nc=new NewsChannelCommands();
		SerialChannelCommands sc=new SerialChannelCommands();
		
		UniversalRemote codaremote=new UniversalRemote();
		
		codaremote.setCommands(1, nc);
		codaremote.setCommands(2, sc);
		
		codaremote.executeCommands(2);
	}
}
class Tv{
	public void switchOn() {
		System.out.println("tv swithced on...");
	}
	public void av1Mode() {
		System.out.println("av1 mode of tv changedd.....");
	}	
}
class SetTopBox{
	public void newsChannel() {
		System.out.println("news channel started....");
	}
	public void serialChannel() {
		System.out.println("serial channel started....");
	}
}
class NetFlix{
	public void login() {	
		System.out.println("netflix login started...");
	}
	public void selectMovies() {
		System.out.println("netflix movie selected...");
	}
}
class AmazonPrime{
	public void login() {
		System.out.println("amazon prime started...");
	}
	public void crossChekWithWebsite() {
		System.out.println("cross check with secret code done...");
	}
	public void typeSecretCode() {
		
	}
}
class SoundSystem{
	public void highSound() {
		System.out.println("high sound....");
	}
	public void lowSound() {
		System.out.println("low sound....");
	}
}