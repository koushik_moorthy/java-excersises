package revision_day5;

public class PaintPgm {
	public static void main(String[] args) {
		PaintBrush pb =new PaintBrush();
		Paint p = new PinkPaint();
		pb.doPaint(p);	
	}
}
class PaintBrush{
	public void doPaint(Paint p) {
		System.out.println(p);
	}
}
abstract class Paint{}
class BluePaint extends Paint{}
class RedPaint extends Paint{}
class GreenPaint extends Paint{}
class PinkPaint extends Paint{}
