package revision_day5;

public class Esa {
	public static void main(String[] args) {
		ESeva es = new ESeva();
		es.setCommand(1, new DeathCerti());
		es.executeCommand(1);
	}
}




class ESeva{
	Command c[] = new Command[5];
	public ESeva() {
	for(int i=0;i<c.length;i++) {
		c[i]= new Dummy();
	}
	}
	public void setCommand(int slot,Command command) {
		c[slot] = command;
	}
	
	public void executeCommand(int slot) {
		c[slot].execute();
	}
}

abstract class Command{
	private Corporation c;
	private Hospital h;
	
	public Corporation getC() {
		return c;
	}
	
	public void setC(Corporation c) {
		this.c=c;
	}
	public Command() {
		c= new Corporation();
		h= new Hospital();

	}
	public abstract void execute();
}

class Dummy extends Command{
	@Override
		public void execute() {
			// TODO Auto-generated method stub
			System.out.println("I am dummy");
		}
}


class DeathCerti extends Command{
	@Override
	public void execute() {
		// TODO Auto-generated method stub
		getC().provideCertificate();
	}
}


class Corporation{
	public void provideCertificate() {
		System.out.println("provide");
	}
}

class Hospital{
	
}
