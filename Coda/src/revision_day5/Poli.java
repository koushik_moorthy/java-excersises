package revision_day5;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class Poli {
	public static void main(String[] args)throws Exception {
		Policestation p = new Policestation();
		Politician nax = new Politician();
		p.arrest(nax);
	}
}


class Policestation
{
	public void arrest(Object p) throws Exception{
		Class c = p.getClass();
		Field field = c.getField("name");
		System.out.println(field.get(p));
		
		Method m = c.getMethod("doWork");
		m.invoke(p);
		
		tortureroom(p);
	}
	public void tortureroom(Object p)throws Exception {
		Class c = p.getClass();
		Field field = c.getDeclaredField("secretname");
		field.setAccessible(true);
		System.out.println(field.get(p));
		
		Method met = c.getDeclaredMethod("doSecretWork");
		met.setAccessible(true);
		met.invoke(p);
	}
}

class Politician{
	public String name = "Ara";
	public void doWork() {
		System.out.println("Do katta panchayat");
	}
	
	private String secretname = "King";
	private void doSecretWork() {
		System.out.println("Bribery");
	}
}

class Naxalite{
	String name = "Nax";
	public void doWork() {
		System.out.println("Drug dealing");
	}
}