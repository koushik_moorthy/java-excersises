package day3.morn;

public class Constructor {
	public static void main(String[] args) {
		Child ch = new Child();
		new Child("Ramu");
		new Child(123);
		System.out.println(ch);
		
	}
}

class Child{
	public Child() {
		System.out.println("Constructor Created");
	}
	public Child(String name) {
		System.out.println(name);
	}
	public Child(int pid) {
		System.out.println("PID is "+pid);
	}
//	@Override
//	public String toString() {
//		// TODO Auto-generated method stub
//		return "Child Object Hash Code "+this.hashCode()+" Class Name "+this.getClass();
//	}
}