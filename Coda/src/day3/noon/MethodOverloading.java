package day3.noon;

public class MethodOverloading {
	public static void main(String[] args) {
		USAHelpLine helper = new USAHelpLine();
		helper.help(new Theft("The thief is Ramu"));
		helper.help(new Domestic("Brothers were Fighting"));
		helper.help(new Civil("Land Problem"));
	}
}

class USAHelpLine{
	public void help(Theft t) {
		System.out.println(t);
	}
	public void help(Domestic d) {
		System.out.println(d);
	}
	public void help(Civil c) {
		System.out.println(c);
	}
}
class Theft{
	String msg;
	public Theft(String message) {
		this.msg= message;
	}
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "Theft caught....."+this.msg;
	}
}

class Domestic{
	String msg;
	public Domestic(String message) {
		this.msg= message;
	}
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "Domestic violence caught....."+this.msg;
	}
}

class Civil{
	String msg;
	public Civil(String message) {
		this.msg= message;
	}
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "Civil Violence caught....."+this.msg;
	}
}

