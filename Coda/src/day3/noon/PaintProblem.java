package day3.noon;

public class PaintProblem {
	public static void main(String[] args) {
		PaintBrush brush = new PaintBrush();
		Paint paint=new BluePaint();
		brush.doPaint(paint);
	}
	
}

class PaintBrush{
	public void doPaint(Paint paint){
		System.out.println(paint);
	}
}

class Paint{}
class RedPaint extends Paint{}
class BluePaint extends Paint{}
class GreenPaint extends Paint{}
class PinkPaint extends Paint{}

