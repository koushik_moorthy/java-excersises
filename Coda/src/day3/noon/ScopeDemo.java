package day3.noon;

public class ScopeDemo {
	static TrainingCenter tc= new TrainingCenter();
	static TrainingCenter tc2= new TrainingCenter();
	public static void main(String[] args) {
		
	}
}

class TrainingCenter{
	static Canteen canteen;// = new Canteen();
	TrainingRoom troom;// = new TrainingRoom();
	
	static void createCanteen() {
		canteen = new Canteen();
	}
	
	void createTrainingRoom() {
		troom = new TrainingRoom();
	}
}

class TrainingRoom{
	public TrainingRoom() {
		System.out.println("Training Room Created");
	}
}

class Canteen{
	public Canteen() {
		System.out.println("Canteen Created");
	}
}

class Student{
	
}