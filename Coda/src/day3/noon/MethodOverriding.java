package day3.noon;

public class MethodOverriding {
	public static void main(String[] args) {
		Child ch =new Child();
		ch.met();
	}
}

class Parent{
	public void met() {
		System.out.println("Parent Called");
	}
//	Parent(){
//		System.out.println("Parent Constructor Called");
//	}
}

class Child extends Parent{
	public void met() {
//		super.met();
		System.out.println("Child Called");
	}
//	Child(){ww
//		System.out.println("Child Constructor Called");
//	}
}