package day13.morn;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.PushbackInputStream;

public class FileIO {
	public static void main(String[] args)throws Exception {
//		new ReadChar().read();
//		new WriteByte().write();
		new Buf().read();
	}
}

class ReadChar{
	public void read() throws Exception{
		FileReader fr = new FileReader("abc.txt");
		char c[] = new char[4];
		int i=0;
		while((i=(fr.read(c)))!=-1) {
			String s = new String(c,0,i);
			System.out.println(s);
		}
//		System.out.println(fr.read());
	}
}


class ReadByte{
	public void read() throws Exception {
		PushbackInputStream fs = new PushbackInputStream(new FileInputStream("abc.txt"));
		System.out.println(fs.available());
		byte b[] = new byte[17];
		fs.read(b,0,17);
		System.out.println(new String(b));
//		int i=0;
//		while((i=fs.read(b))!=-1) {
//			String s = new String(b,0,i);
//			System.out.println(s);
//		}
	}
}

class Buf{
	public void read() throws Exception{
		BufferedInputStream bufin = new BufferedInputStream(System.in);
		int data= bufin.read();
		int dat;
		bufin.mark(3);
		byte b[] = new byte[3];
		System.out.println(bufin.read(b));
		bufin.reset();
		data = bufin.read();
		System.out.println((char)data);
	}
}



class WriteByte{
	public void write() throws Exception{
		
		BufferedInputStream fis = new BufferedInputStream(new FileInputStream("abc.txt"));
		BufferedOutputStream fos = new BufferedOutputStream(new FileOutputStream("copied1.txt"));
		byte[] b = new byte[4];
		int i=0;
		while(((i=fis.read(b))!=-1)) {
			fos.write(b,0,i);
		}
		fos.flush();
		fos.close();
	}
}