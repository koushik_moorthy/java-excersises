package day13.morn;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class SerializationDemo {
	public static void main(String[] args) throws Exception {
		Laddu laddu = new Laddu();
		laddu.size=10;
		new WriteObject().createDeepCopy(laddu);
//		System.out.println("Size of laddu "+laddu.size);
		laddu.size=5;
//		System.out.println("Size of laddu "+laddu.size);
		laddu = new ReadObject().getDeepCopy();
		System.out.println("Magic "+laddu.size);
	}
}

class Laddu implements Serializable{
	int size=0;
}

class WriteObject{
	public void createDeepCopy(Laddu laddu) throws Exception{
		try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("laddu.dat"))){
			oos.writeObject(laddu);
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}
}

class ReadObject{
	public Laddu getDeepCopy(){
		try(ObjectInputStream ois = new ObjectInputStream(new FileInputStream("laddu.dat"))){
			return (Laddu)ois.readObject();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
		
	}
}