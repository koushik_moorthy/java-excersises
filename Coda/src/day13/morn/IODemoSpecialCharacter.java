package day13.morn;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;

public class IODemoSpecialCharacter {
	public static void main(String[] args) {
		//new Reader().read("Hello &copy; world");
		new Reader().read("Hello &copy world");
	}
	}
	class Reader {
		public void read(String str) {
			//try with resources
			try(BufferedReader br=new BufferedReader(new InputStreamReader(new ByteArrayInputStream(str.getBytes()))))
					{
						int i=0;
						boolean mark=false;
						while((i=br.read())!=-1) {
							switch(i) {
							case '&':
							{
								mark=true;
								br.mark(50);
								break;
							}
							case ';' : {
								System.out.print((char)169);
								mark=false;
								break;
							}
							case ' ': {
								if(mark) {
									br.reset();
									mark=false;
									System.out.print("&");
								}
								else
									System.out.print(" ");
								break;
							}
							default: {
								if(!mark)
									System.out.print((char)i);
							}
							}
						}
					}
			catch(Exception e) {
				e.printStackTrace();
			}
		}
	}