package day13.noon;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;

public class ServerProgram {
	public ServerProgram() {
		Socket s;
		ServerSocket ss;
		try {
			ss= new ServerSocket(2400);
			System.out.println("Server ready to accept...");
			s = ss.accept();
			System.out.println(s);
			//send
			BufferedInputStream buff = new BufferedInputStream(s.getInputStream());
//			System.out.println((char)buff.read());
			//receive
			
			BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
			while((br.read())!=-1) {
				System.out.println(br.readLine());
			}
			s.close();
			
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	public static void main(String[] args) {
		new ServerProgram();
	}
	
}
