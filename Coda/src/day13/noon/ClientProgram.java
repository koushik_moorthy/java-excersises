package day13.noon;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.Socket;

public class ClientProgram {
	Socket s;
	public ClientProgram() {
		try {
		s = new Socket("localhost", 2400);
		System.out.println("Connected");
		String b;
		//send
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		while((br.read())!=-1) {
			System.out.println(br.readLine());
		}
		//receive
		BufferedInputStream buff = new BufferedInputStream(s.getInputStream());
//		System.out.println((char)buff.read());
//		System.out.println(s);
		s.close();
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		
	}
	public static void main(String[] args) {
		new ClientProgram();
	}
}
