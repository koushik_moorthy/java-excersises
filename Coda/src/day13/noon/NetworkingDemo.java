package day13.noon;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Inet4Address;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.net.URLClassLoader;

public class NetworkingDemo {
	public static void main(String[] args) throws Exception {
//		URL url = new URL("http://google.com/index.html");
//		URLConnection urlcon = url.openConnection();
//		
//		BufferedReader buffreader = new BufferedReader(new InputStreamReader(urlcon.getInputStream()));
//		BufferedWriter buffwriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("google.html"),Charset.forName("UTF-8")));
//		
//		int i = 0;
//		
//		while((i=buffreader.read())!=-1) {
//			System.out.print((char)i);
//			buffwriter.write((char)i);
//			
//		}
//		buffwriter.close();
//		
//		
		InetAddress[] inet = Inet4Address.getAllByName("smarttuitions.in");
		for(InetAddress in:inet) {
			System.out.println(in.getHostAddress());
		}
	}
}
