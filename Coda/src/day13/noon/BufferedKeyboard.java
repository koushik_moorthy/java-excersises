package day13.noon;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.InputStreamReader;

public class BufferedKeyboard {
	public static void main(String[] args)throws Exception {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter a string....");
		int sr = Integer.parseInt(br.readLine());
		int dr = Integer.parseInt(br.readLine());
		int add = sr+dr;
		System.out.println(add);
	}
}
