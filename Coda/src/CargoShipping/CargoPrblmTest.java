package CargoShipping;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.Test;

public class CargoPrblmTest {

	@Test
	public void test() {
		CargoShip c1=new CargoShip(LocalDate.now(),LocalTime.of(11,0),24);
		c1.calDayofDelivery();
		assertTrue((LocalDate.of(2020, 9, 24).equals(c1.Deliverydate)));
		assertTrue((LocalTime.of(11, 0).equals(c1.Deliverytime)));
		
//		CargoShip c2=new CargoShip(LocalDate.now(),LocalTime.of(12,0),24);
//		c2.calDayofDelivery();
//		assertTrue((LocalDate.of(2020, 9, 24).equals(c2.Deliverydate)));
//		assertTrue((LocalTime.of(12, 0).equals(c2.Deliverytime)));
	}
}
