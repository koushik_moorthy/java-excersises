package CargoShipping;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.Locale;
import java.util.Scanner;
import java.util.TimeZone;

public class CargoShipShipment {
	public static void main(String[] args) {
		int nohrs;
		System.out.println("Enter no of hrs....");
		Scanner scan = new Scanner(System.in);
		nohrs = scan.nextInt();
		ZoneId zoneId = ZoneId.of("Asia/Kolkata");
		System.out.println(zoneId);
		CargoShip c1=new CargoShip(LocalDate.now(),LocalTime.now(),nohrs);
		c1.calDayofDelivery();
	}
	
	public void setDetails(LocalDate sdate, int noofhrs, LocalDate d, LocalTime t) {
		System.out.println("Shipped at  :"+sdate+" "+LocalTime.now());
		System.out.println("Travel hours: "+noofhrs);
//		System.out.println("Estimated date of delivery and Time: "+CargoShip.dateConv(d)+" & "+t);
		System.out.println("Estimated date of delivery and Time: "+d+" & "+t);
	}
}

class CargoShip{
	LocalDate Shipmentdate;
	LocalTime time;
	int NoOfHours;
	LocalDate Deliverydate;
	LocalTime Deliverytime;
	LocalTime end= LocalTime.of(20,0,0);	
	public LocalDate CheckBusinessDay(LocalDate d) {
		if(d.getDayOfWeek().getValue()==6 )
			d=d.plus(1,ChronoUnit.DAYS);
		if(d.getDayOfWeek().getValue()==7)
			d=d.plus(1,ChronoUnit.DAYS);
		
		for(holidays i:holidays.values()) {
			if(d.equals(i.getlocaldate()))
				d=d.plus(1,ChronoUnit.DAYS);
		}
		return d;
	}
	
	public CargoShip(LocalDate d,LocalTime t,int hrs) {
		this.Shipmentdate=d;
		this.time=t;
		this.NoOfHours=hrs;
	}
	
	public void calDayofDelivery() {
		LocalDate ddate=Shipmentdate;
		LocalTime start=time;
		float hours=NoOfHours;
		
		
		while(hours>=0) {
			ddate=CheckBusinessDay(ddate);
			if(time.isAfter(start) && time.isAfter(end)) {
				start=LocalTime.of(8,0,0);
				Duration duration = Duration.between(start, end);
				if(hours<=duration.toHours()) {
					start=start.plus((long) hours,ChronoUnit.HOURS);
				}
				hours=hours-duration.toHours();
				if(hours>=0) {
					ddate=ddate.plus(1,ChronoUnit.DAYS);
				}
			}
			else {
				start=LocalTime.of(8,0,0);
				ddate =ddate.plus(1,ChronoUnit.DAYS);
			}
		}
		Deliverydate = ddate;
		Deliverytime = start;
		CargoShipShipment css = new CargoShipShipment();
		css.setDetails(Shipmentdate,NoOfHours,Deliverydate,Deliverytime);
	}
	public static Date dateConv(LocalDate date) {
	    return Date.from(date.atStartOfDay().toInstant(ZoneOffset.UTC));
	  }
}

enum holidays{
	
	NEWYEAR(LocalDate.of(LocalDate.now().getYear(), 01, 01)),
	REPUBLICDAY(LocalDate.of(LocalDate.now().getYear(), 01, 26)),
	INDEPENDENCEDAY(LocalDate.of(LocalDate.now().getYear(), 8, 15));
	
	private LocalDate locdat;
	
	holidays(LocalDate locd) {
		this.locdat=locd;
	}
	
	LocalDate getlocaldate() {
		return this.locdat;
	}
}