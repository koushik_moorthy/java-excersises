package day6.morning;

public class ExceptionDemo {
	public static void main(String[] args) {
		System.out.println("Before Exception");
		try {
			int i=1/Integer.parseInt(args[0]);
		}
		catch(ArithmeticException ae) {
			System.out.println(ae);
		}
		catch(ArrayIndexOutOfBoundsException aiob) {
			System.out.println(aiob);
		}
		catch(NumberFormatException nfe) {
			System.out.println(nfe);
		}
		finally {
			System.out.println("Finally Called");
		}
		System.out.println("After Exception");
	}
}
