package day6.morning;
public class Corruption {
	public static void main(String[] args) {
		ESeva es = new ESeva();
		es.setCommand(1, new DeathCertificateCommand());
		es.executeCommand(1);
		
	}
}

class ESeva{
	Command c[]=new Command[5];
	
	public ESeva() {
	for(int i=0; i<5;i++) {
		c[i]= new Dummy();
	}
	}
	public void setCommand(int slot, Command command) {
		c[slot] = command;
	}
	
	public void executeCommand(int slot) {
		c[slot].execute();
	}
}

abstract class Command{
	private Corporation c;
	private Hospital h;
	private Police p;
	
	public Corporation getC() {
		return c;
	}
	public void setC(Corporation c) {
		this.c=c;
	}
	public Hospital getH() {
		return h;
	}
	public void setC(Hospital h) {
		this.h=h;
	}
	public Police getP() {
		return p;
	}
	public void setP(Police p) {
		this.p=p;
	}
	
	public Command() {
		c= new Corporation();
		p = new Police();
		h= new Hospital();
	}
	
	public abstract void execute();
	
}

class Dummy extends Command{
	@Override
	public void execute() {
		// TODO Auto-generated method stub
		System.out.println("I am dummy not operational");
	}
}

class DeathCertificateCommand extends Command{
	@Override
	public void execute() {
		// TODO Auto-generated method stub
		System.out.println("Starting the process");
		getP().doInvestigation();
		getH().doPostmoterm();
		getC().provideCertificate();
		System.out.println("Certificate given");
	}
}



class Corporation {
	public void provideCertificate() {
		System.out.println("Provide Death Certificate");
	}
}

class Hospital{
	public void doPostmoterm() {
		System.out.println("Do Postmorterm");
	}
}

class Police{
	public void doInvestigation() {
		System.out.println("Do Investigation");
	}
}