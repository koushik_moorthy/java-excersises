package day6.morning;

import java.util.Scanner;

public class CustomException {
	public static void main(String[] args) {
		TestCustomException tce = new TestCustomException();
		try {
			tce.test();
			
		}
		catch(Throwable t) {
			System.out.println(t);
		}
	}
}

class TestCustomException{
	public void test() throws Throwable{
		Scanner scan = new Scanner(System.in);
		System.out.println("Enter value of text");
		String in = scan.next();
		if(in.equals("Ramu")) {
			throw new MyException("This is custom Exception");
		}
		
	}
}

class MyException extends Throwable{
	String msg;
	public MyException(String msg) {
		// TODO Auto-generated constructor stub
		this.msg = msg;
	}
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "My Custom Exception String is......"+msg;
	}
}
