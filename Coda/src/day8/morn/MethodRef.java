package day8.morn;

public class MethodRef {
	public static void main(String[] args) {
		Fun1 fadd =new AddBazzar()::add;
		System.out.println(fadd.add(20, 30));
		Fun1 fstadd = AddBazzar::addstatic;
		System.out.println(fstadd.add(120, 30));
	}
}


interface Fun1{
	public int add(int i,int j);
}


class AddBazzar{
	public int add(int i,int j) {
		return i+j;
	}
	public static int addstatic(int i,int j) {
		return i+j;
	}
}