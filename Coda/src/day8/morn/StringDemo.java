package day8.morn;

public class StringDemo {
	public static void main(String[] args) {
		Demonstrate d= new Demonstrate();
		
	}
}


class Demonstrate{
	public Demonstrate() {
		String str1 = "Hello";
		String str2 = "Hello";
		
		System.out.println(str1);
		System.out.println(str2);
		
		String st1 = "World";
		String st2 = "World";
		
		System.out.println(st1);
		System.out.println(st2);
		
		System.out.println(st1.equals(st2));
		
		char c[]= {'a','c','f'};
		
		String cstr = new String(c,0,1);
		
		System.out.println(cstr);
		
		StringBuilder sb1 = new StringBuilder("Hello ");
		sb1.append("World");
		
		System.out.println(sb1);
		
		StringBuffer sb2 = new StringBuffer("Molly ");
		sb2.append("Dolly");
		
		System.out.println(sb2);
		
		work(1,2,3,100,200);
		
		String str = String.format("hello string %s",str1);
		System.out.println(str);
		
		System.out.printf("hello string %s",st2);
		
		
		System.out.printf("Integer : %d\n",15);
		System.out.println();
		System.out.printf("Floating point numbers...%f",12.21111111);
		System.out.println();
		System.out.printf("Floating point numbers with 8 place...%.8f",12.21111111);
		System.out.println();
		System.out.printf("String %s Integer %06d Float point numbers...%f","Hello World",89,12.21111111);
		System.out.println();
		System.out.printf("%-12s%-12s%-12s","Column1","Column2","Column3");
		System.out.println();
		System.out.printf("%-12.5s%s", "Hello World", "World");
		
	}
	
	public void work(int ...args) {
		for(int i:args) {
			System.out.println(i);
		}
	}
}