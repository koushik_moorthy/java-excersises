package day8.noon;

public class ThreadDemo {
	public static void main(String[] args) throws Exception{
		Thread t = Thread.currentThread();
		t.setName("koushik");
		t.setPriority(10);
		for(int i=0;i<5;i++) {
			System.out.println(i);
			Thread.sleep(0);
		}
		Thread t2 = new Thread(()->{System.out.println("Runnable thread");});
		t2.run();
		new Thread(new Runnable() {
			@Override
			public void run() {
				// TODO Auto-generated method stub
				System.out.println("Runnable anonymous");
			}
		}).run();
		
		Thread t3=new Thread(new Run());
		t3.run();
	}
}


class Run implements Runnable{
	@Override
	public void run() {
		// TODO Auto-generated method stub
		System.out.println("Runnable method in traditional way");
	}
}