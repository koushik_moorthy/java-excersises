package day10.morn;

import java.util.Optional;

public class OptionalClasses {
	public static void main(String[] args) {
		OptionalImp oi = new OptionalImp();
	}
}


class OptionalImp{
	public OptionalImp() {
		Optional<String> o1 = Optional.of("Koushik");
		System.out.println(o1);
		Optional<String> o2 = Optional.ofNullable(null);
		System.out.println(o2);
		
		System.out.println(o1.equals(o2));
		
		Optional <Object> o3 =Optional.empty();
		System.out.println(o3);
		
		System.out.println(o1.hashCode());
		
		String name = "bblaaaa";
		String name2 = Optional.ofNullable(name).orElse("jos");
		System.out.println(name2);
	}
}