package day15.morn;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.LongStream;
import java.util.stream.Stream;

public class StreamDemo {
	public static void main(String[] args) {
		List<Integer> ls = new ArrayList<>();
		ls.add(1);
		ls.add(4);
		ls.add(5);
		ls.add(12);
		ls.add(10);
		
//		ls = ls.stream().skip(3).collect(Collectors.toList());
//		System.out.println(ls);
//		
//		int reduced	=ls.stream().reduce((n1,n2)->n1+n2).get();
//		System.out.println(reduced);
		
//		ls.stream().sorted((o1,o2)->new MyComparator().compare(o1, o2)).forEach(System.out::println);
		
//		int min  = ls.stream().min((n1,n2)->n1.compareTo(n2)).get();
//		System.out.println(min);
//		
//		int max = ls.stream().max((n1,n2)->n1.compareTo(n2)).get();
//		System.out.println(max);
//		
//		int key = 12;
//		int arr[] = {1,5,2,0,7,9};
//		boolean res = IntStream.of(arr).anyMatch(n1->(n1==key));
//		if(res) {
//			System.out.println("Element Found");
//		}
//		else {
//			System.out.println("Element not Found");
//		}
		
//		long arr1[]= {9,2,4,5,7};
//		double avg = LongStream.of(arr1).average().getAsDouble();
//		System.out.println(avg);
//	
//		Stream str = Stream.of(1,2,4,2,4,9,2,1).distinct();
//		str.forEach(System.out::println);
		
//		Stream str1 = Stream.of(90,10,70,50,60,20,10,30).filter(n1->(n1>35)).map(n1->(n1+2));
//		str1.forEach(System.out::println);
		
		Stream str1 = Stream.of("aaa","bbb","ccc");
		Stream str2 = Stream.of("ddd","eee","fff");
		Stream s = Stream.concat(str1, str2);
		s.forEach(System.out::println);
//		
//		Stream str1 = Stream.of(1,5,3,6,7,9);
//		str1.peek(System.out::println).count();
		
		
	}
}

class MyComparator implements Comparator{
	@Override
	public int compare(Object o1, Object o2) {
		// TODO Auto-generated method stub
		Integer i1 = (Integer)o1;
		Integer i2 = (Integer)o2;
		return -i1.compareTo(i2);
	}
}