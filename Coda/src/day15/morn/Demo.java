package day15.morn;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Demo {
	public static void main(String[] args) {
		List<Integer> ls = new ArrayList<>();
		ls.add(1);
		ls.add(2);
		ls.add(4);
		ls.add(0);
		ls.add(8);
		
		int i = ls.stream().sorted().reduce((n1,n2)->n1+n2).get();
		IntStream.range(2, 8).forEach(System.out::println);
		
		System.out.println(i);
		
	}
}
