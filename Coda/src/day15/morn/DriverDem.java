package day15.morn;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class DriverDem {
	public static void main(String[] args) throws Exception {
		Class.forName("com.mysql.cj.jdbc.Driver");
		String username="root";
		String password="koushik07";
		String uri = "jdbc:mysql://localhost/coda";
		Connection con = DriverManager.getConnection(uri,username,password);
		System.out.println("Connection establised....");
		Statement st = con.createStatement();
		ResultSet rs = st.executeQuery("select * from students");
		while(rs.next()) {
			System.out.println(rs.getInt(1)+"......"+rs.getString(2)+"........"+rs.getInt(3)+"......"+rs.getInt(4));
		}
		con.close();
	}
}
