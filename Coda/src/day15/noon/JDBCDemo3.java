package day15.noon;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;

import com.mysql.cj.jdbc.CallableStatement;

public class JDBCDemo3 {
	public static void main(String[] args) throws Exception {
		Connection con= ConnectionPool.getConnection();
		
		CallableStatement call = (CallableStatement) con.prepareCall("{call setFlagOneToAll}");
		int rowsupdated = call.executeUpdate();
		System.out.println("Rows Updated...."+rowsupdated);

		call = (CallableStatement) con.prepareCall("{call setFlagToAll(?)}");
		call.setInt(1,0);
		rowsupdated = call.executeUpdate();
		System.out.println("Rows Updated...."+rowsupdated);
		
		call = (CallableStatement) con.prepareCall("call setFlagToStudent(?,?)");
		call.setInt(1, 1);
		call.setInt(2, 100);
		rowsupdated = call.executeUpdate();
		System.out.println("Rows Updated...."+rowsupdated);
		
		call = (CallableStatement) con.prepareCall("{call getFlag(?,?)}");
		call.setInt(1, 100);
		call.registerOutParameter(2, Types.INTEGER);
		call.execute();
		int flag=call.getInt(2);
		System.out.println("Flag value is....."+flag);
	}
}
