package day15.noon;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.mysql.cj.jdbc.result.ResultSetMetaData;

public class JDBCDemo4 {
	public static void main(String[] args) throws Exception {
		Connection con = ConnectionPool.getConnection();
		Statement st = con.createStatement();
		
		boolean rs = st.execute("select * from students");
		System.out.println(rs);
		
		st= con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);
		
		
		 
		ResultSet rset = st.executeQuery("select * from students");
//		rset.afterLast();
//		while(rset.previous()) {
//			System.out.println(rset.getInt(1));
//		}
		ResultSetMetaData rsmd = (ResultSetMetaData) rset.getMetaData();
		int colcount = rsmd.getColumnCount();
		for(int i=1;i<=colcount;i++) {
			System.out.print(rsmd.getColumnName(i)+"\t\t");
		}
		System.out.println("\n.................................................");
		while(rset.next()) {
			for(int i=1;i<=colcount;i++) {
				System.out.print(rset.getString(i)+"\t\t");
			}
			System.out.println();
		}
	}
}
