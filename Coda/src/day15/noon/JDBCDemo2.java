package day15.noon;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.mysql.cj.xdevapi.PreparableStatement;

public class JDBCDemo2 {
	public static void main(String[] args) throws Exception{
		Connection con = ConnectionPool.getConnection();
		
		BufferedReader bir = new BufferedReader(new InputStreamReader(System.in));
		PreparedStatement ps = con.prepareStatement("select * from students where sid=?");
		while(true) {
			System.out.println("Enter sid");
			int sid = Integer.parseInt(bir.readLine());
			ps.setInt(1, sid);
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				System.out.println(rs.getString(2));
			}
		}
//		ConnectionPool.removeConnection();
	}
}
