package day15.noon;

import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.util.logging.Logger;
import javax.sql.ConnectionEventListener;
import javax.sql.ConnectionPoolDataSource;
import javax.sql.PooledConnection;
import javax.sql.StatementEventListener;
import java.sql.Connection;
import java.sql.DriverManager;



public class ConnectionPool {
	private void ConnectionPool() {}
	private static Connection con;
	static {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	private static String userid="root";
	private static String password="koushik07";
	private static String uri="jdbc:mysql://localhost/coda";
	private static final ThreadLocal<Connection> tLocal=new ThreadLocal<Connection>();
	synchronized public static Connection getConnection() {
			con=tLocal.get();
			if(con==null) {
				try {
					con=DriverManager.getConnection(uri,userid,password);
					tLocal.set(con);
//					System.out.println(con);
					return con;
				}catch(Exception e) {
					e.printStackTrace();
					return null;
				}
			}
			return con;
	}
	synchronized public static void removeConnection() {
		con=tLocal.get();
		if(con!=null) {
			try {
			con.close();
			tLocal.remove();
			}catch(Exception e) {
				e.printStackTrace();
			}
		}
	}
	public static void main(String[] args) {
		getConnection();
	}
}