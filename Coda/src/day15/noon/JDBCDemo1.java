package day15.noon;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class JDBCDemo1 {
	public static void main(String[] args) throws Exception {
		Connection con = ConnectionPool.getConnection();
		Statement st = con.createStatement();
		
		//update
		int affected = st.executeUpdate("update students set rno=0");
		System.out.println("Columns affected......"+affected);
		
		
		//select
		ResultSet rs = st.executeQuery("select* from students");
		while(rs.next()) {
			System.out.println(rs.getInt("rno")+"......."+rs.getString(2)+"........"+rs.getString(3)+".........."+rs.getString(4));
		}
		
		ResultSet rs1 = st.executeQuery("select* from students");
		while(rs1.next()) {
			
		}
		
		ConnectionPool.removeConnection();
	}
}
