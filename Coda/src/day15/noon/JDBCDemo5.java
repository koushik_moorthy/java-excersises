package day15.noon;

import java.sql.Connection;
import java.sql.SQLException;

import com.mysql.cj.jdbc.DatabaseMetaData;

public class JDBCDemo5 {
	public static void main(String[] args) throws Exception {
		Connection con = ConnectionPool.getConnection();
		DatabaseMetaData dbmd = (DatabaseMetaData) con.getMetaData();
		System.out.println(dbmd.getDatabaseProductName());
		System.out.println(dbmd.getDriverName());
		System.out.println(dbmd.getDefaultTransactionIsolation());
		System.out.println(dbmd.getDriverMajorVersion());
		ConnectionPool.removeConnection();
	}
}
