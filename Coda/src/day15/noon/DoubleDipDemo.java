package day15.noon;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class DoubleDipDemo {
	public static void main(String[] args) {
		ExecutorService es= Executors.newFixedThreadPool(1);
		
		es.execute(()->DoubleDip.getDoubleDip());
		es.execute(()->DoubleDip.getDoubleDip());
	}
}


class DoubleDip {
	
	private DoubleDip() {
		System.out.println("Cons called");
	}
	private static DoubleDip dd;
	synchronized public static DoubleDip getDoubleDip() {
		if(dd==null) {
			try {Thread.sleep(1000);}catch(Exception e) {e.printStackTrace();}
			dd= new DoubleDip();
		}
		return dd;
	}
	
}