package day11.noon;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

public class ObserverDemo {
	public static void main(String[] args) {
		FireAlarm fa = new FireAlarm();
		
		Student std = new Student();
		Teacher tea = new Teacher();
		
		fa.addObserver(std);
		fa.addObserver(tea);
		fa.ringAlarm();
				
	}
}

class FireAlarm extends ThreadedObservable{
	public void ringAlarm() {
		setChanged();
		notifyObservers("Fire in the building");
	}
}

class Student implements Observer{
	@Override
	public void update(Observable observer, Object obj) {
		// TODO Auto-generated method stub
		System.out.println((String)obj);
		runFast();
	}
	public void runFast() {
		System.out.println("Student: Run very fast");
	}
}

class Teacher implements Observer{
	@Override
	public void update(Observable obs, Object arg) {
		// TODO Auto-generated method stub
		System.out.println((String)arg);
		System.out.println("Teacher: Am thinking");
		try {Thread.sleep(4000);}catch(Exception e) {}
		walkFast();
	}
	public void walkFast() {
		System.out.println("Teacher: Walk very fast");
	}
}


class ThreadedObservable extends Observable{
	ArrayList<Observer> list = new ArrayList<>();
	
	@Override
	public void notifyObservers(Object arg) {
		// TODO Auto-generated method stub
		for(Observer o:list) {
			new Thread(()->{o.update(this, arg);}).start();
		}
	}
	@Override
	public synchronized void addObserver(Observer o) {
		// TODO Auto-generated method stub
		list.add(o);
	}
}