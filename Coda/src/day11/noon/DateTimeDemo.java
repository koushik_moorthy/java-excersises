package day11.noon;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import java.time.Year;
import java.time.temporal.ChronoUnit;

public class DateTimeDemo {
	public static void main(String[] args) {
		LocalDate ld = LocalDate.now();
		System.out.println(ld);
		
		LocalDate ldw = ld.plus(1,ChronoUnit.WEEKS);
		System.out.println(ldw);
		
		LocalDate ldd = ld.plus(1,ChronoUnit.DECADES);
		System.out.println(ldd);
		
		LocalDate lddemo = ld.plusYears(5);
		System.out.println(lddemo);
		
		LocalTime lt = LocalTime.now();
		System.out.println(lt);
		
		Duration dt = Duration.ofHours(1);
		LocalTime lt2 = lt.plus(dt);
		System.out.println(lt2);
		
		Duration dbtw = Duration.between(lt, lt2);
		System.out.println(dbtw);
		
		LocalDateTime ldt = LocalDateTime.now();
		System.out.println(ldt);
		
		
		LocalDate locdate = ldt.toLocalDate();
		System.out.println(locdate);
		
		
		int date = ldt.getDayOfMonth();
		Month mon = ldt.getMonth();
		int	 year = ldt.getYear();
		System.out.println(date);
		System.out.println(mon);
		System.out.println(year);
	}
}
