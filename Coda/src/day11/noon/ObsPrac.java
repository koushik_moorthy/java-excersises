package day11.noon;

import java.util.Observable;
import java.util.Observer;

public class ObsPrac {
	public static void main(String[] args) {
		FireAlarms fa = new FireAlarms();
		Student st = new Student();
		
		fa.addObserver(st);
		fa.ringAlarm();
	}
}

class FireAlarms extends Observable{
	public void ringAlarm() {
		setChanged();
		notifyObservers("Fire fire fire");
	}
}

class Students implements Observer{
	@Override
	public void update(Observable o, Object arg) {
		// TODO Auto-generated method stub
		System.out.println((String)arg);
		runFast();
	}
	public void runFast() {
		System.out.println("Running Fast");
	}
}