package day11.morning;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class ColDemo5 {
	public static void main(String[] args) {
		Map<String, String> map = new HashMap<String,String>();
		map.put("a", "HelloCoda");
		map.put("b", "HelloCoda-1");
		map.put("c", "HelloCoda-2");
		
		System.out.println(map);
		
		System.out.println(map.get("b"));
		
		Set<Map.Entry<String, String>> set = map.entrySet();
		
		Iterator<Map.Entry<String, String>> iter = set.iterator();
		
		while(iter.hasNext()) {
			Map.Entry<String, String> me = iter.next();
			System.out.println(me.getKey()+":"+me.getValue());
		}
	}
}
