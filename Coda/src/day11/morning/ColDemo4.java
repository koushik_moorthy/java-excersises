package day11.morning;

import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.ListIterator;
import java.util.Set;
import java.util.TreeSet;

public class ColDemo4 {
	public static void main(String[] args) {
//		Set<String> stt=new TreeSet<>((String o1,String o2)->{return o1.compareTo(o2);});
//		Set<String> sttt= new TreeSet<>(new Comparator<String>() {
//			@Override
//			public int compare(String o1, String o2) {
//				// TODO Auto-generated method stub
//				return o2.compareTo(o1);
//			}
//		});
		
		
		
//		Set<String> st = new HashSet<String>(10,10);
//		
//		st.add("aaaa");
//		st.add("bbb");
//		st.add("ccc");
		Set<Employee1> st =new TreeSet<Employee1>(new MyComparator1());
		st.add(new Employee1("Tamu",40));
		st.add(new Employee1("Ramu",20));
		st.add(new Employee1("Bamu",40));
		st.add(new Employee1("Samu",30));
		
		System.out.println(st);
		
		
		
//		Iterator<Employee1> it = st.iterator();
//		while(it.hasNext()) {
//			System.out.println(it.next());
//		}
		
	}
}

class MyComparator1 implements Comparator<Employee1>{
	@Override
	public int compare(Employee1 o1, Employee1 o2) {
		// TODO Auto-generated method stub
		return (o2.compareTo(o1));
	}
}

class Employee1 implements Comparator<Employee1>{
	String name;
	int age;
	@Override
	public int compare(Employee1 o1, Employee1 o2) {
		// TODO Auto-generated method stub
		return 0;
	}
	public Employee1(String name,int age) {
		this.name=name;
		this.age=age;
	}
	@Override
	public String toString() {
		return name+"   "+age;
	}
	
	public int compareTo(Employee1 o) {
		// TODO Auto-generated method stub
		return this.name.compareTo(o.name);
	}
}
