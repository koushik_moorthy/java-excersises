package day11.morning;

import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.ListIterator;
import java.util.Set;
import java.util.TreeSet;

public class ColDemo3 {
	public static void main(String[] args) {
		Set<String> stt=new TreeSet<>((String o1,String o2)->{return o1.compareTo(o2);});
		Set<String> sttt= new TreeSet<>(new Comparator<String>() {
			@Override
			public int compare(String o1, String o2) {
				// TODO Auto-generated method stub
				return o2.compareTo(o1);
			}
		});
		
		Set<String> st =new TreeSet<String>(new MyComparator());
		
//		Set<String> st = new HashSet<String>(10,10);
		
		st.add("aaaa");
		st.add("bbb");
		st.add("ccc");
		
		System.out.println(st);
		
		for(String i:st) {
			System.out.println(i);
		}
		
		Iterator<String> it = st.iterator();
		while(it.hasNext()) {
			System.out.println(it.next());
		}
		
	}
}

class MyComparator implements Comparator<String>{
	@Override
	public int compare(String o1, String o2) {
		// TODO Auto-generated method stub
		return o2.compareTo(o1);
	}
}