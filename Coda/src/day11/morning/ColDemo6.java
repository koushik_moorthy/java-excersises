package day11.morning;

import java.util.HashMap;
import java.util.WeakHashMap;

public class ColDemo6 {
	public static void main(String[] args) {
		WeakHashMap<Key, String> hm = new WeakHashMap<Key, String>();
		Key a1 =new Key("A2");
		Key a2 =new Key("A22");
		
		hm.put(a1,"Hello");
		hm.put(a2,"World");
		
		System.out.println(hm);
		a1=null;
		System.gc();
		System.out.println(hm);
	}
}

class Key{
	String id;
	public Key(String id) {
		this.id = id;
	}
}