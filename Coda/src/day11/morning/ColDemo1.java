package day11.morning;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Vector;

public class ColDemo1 {
	public static void main(String[] args) {
		List<String> ls = new ArrayList<>();
		Vector <String> vec= new Vector<>();
		ls.add("1abcd");
		ls.add("2efgh");
		
		for(int i=0;i<ls.size();i++) {
			System.out.println(ls.get(i));
		}
		
		for(String s:ls) {
			System.out.println(s);
		}
		
		Iterator<String> itr = ls.iterator();
		while(itr.hasNext()) {
			System.out.println(itr.next());
		}
		
		ListIterator<String> listiter = ls.listIterator();
		
		while(listiter.hasNext()) {
			System.out.println(listiter.next());
		}
		
		Enumeration<String> en = vec.elements();
		vec.add("Aaa");
		while(en.hasMoreElements()) {
			System.out.println(en.nextElement());
		}
		
	}
}
