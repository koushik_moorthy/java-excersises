package day11.morning;

import java.util.Stack;

public class ColDemo2 {
	public static void main(String[] args) {
		Stack<String> st = new Stack<>();
		st.add("1");
		st.add("2");
		
		st.push("3");
		
		System.out.println(st);
		
		for(String i:st) {
			System.out.println(i);
		}
		System.out.println("Searching......."+st.search("1"));
		System.out.println("peeking top......"+st.peek());
		System.out.println("_Pop Operation_");
		for(int i=st.size();i>0;i--) {
			System.out.println(st.pop());
		}
		System.out.println(st);
	}
}
