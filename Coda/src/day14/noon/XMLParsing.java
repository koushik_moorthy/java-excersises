package day14.noon;


import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class XMLParsing {
	public static void main(String[] args) throws Exception {
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		dbf.setIgnoringElementContentWhitespace(true);
		DocumentBuilder db = dbf.newDocumentBuilder();
		
		Document d = db.parse("books.xml");
		
		Element rootelement = d.getDocumentElement();
		
		System.out.println("Root name...."+rootelement.getNodeName());
		
		System.out.println("Root size is...."+rootelement.getChildNodes().getLength());
		
		
	}
}
