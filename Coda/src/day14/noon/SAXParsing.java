package day14.noon;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class SAXParsing {
	public static void main(String[] args) throws Exception {
		SAXParserFactory sxpf = SAXParserFactory.newInstance();
		SAXParser sxp = sxpf.newSAXParser();
		sxp.parse("books.xml", new MyDefaultHandler());
	}
}
class MyDefaultHandler extends DefaultHandler{
	@Override
	public void startDocument() throws SAXException {
		// TODO Auto-generated method stub
		System.out.println("Parsing started");
	}
	@Override
	public void endDocument() throws SAXException {
		// TODO Auto-generated method stub
		System.out.println("Parsing Ended");
	}
	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
		// TODO Auto-generated method stub
		System.out.println(qName);
	}
	@Override
	public void characters(char[] ch, int start, int length) throws SAXException {
		// TODO Auto-generated method stub
		String s = new String(ch,start,length);
//		System.out.println("Start end"+start+""+ch+""+length);
		System.out.println(s);
		new Prison().openGate(s);
	}
}
class Prison{
	public void openGate(String s) {
		switch(s) {
		case "Java":
			System.out.println("Open the gate");
		}
	}
}