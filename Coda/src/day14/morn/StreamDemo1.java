package day14.morn;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.LongStream;
import java.util.stream.Stream;

public class StreamDemo1 {
	public static void main(String[] args) throws URISyntaxException, IOException {
		StreamDemo1 str = new StreamDemo1();
//		str.normalCode();
//		str.reducedCode();
//		str.createStream();
//		str.checkPass();
		str.checkMinMax();
		
	}
	public void normalCode() {
		List<String> ls = new ArrayList<>();
		ls.add("aaa");
		ls.add("bbb");
		
		for(String s:ls) {
			System.out.println(s);
		}
	}
	
	public void reducedCode() {
		List<Integer> ls = Arrays.asList(5,6,7,8);
//		ls.stream().forEach((i)->{System.out.println(i);});
		ls.stream().map(i->i+5).collect(Collectors.toList()).forEach(System.out::println);
		
//		ls.stream().forEach(System.out::println);
//		ls.parallelStream().forEach(System.out::println);
//		ls.parallelStream().forEachOrdered(System.out::println);
	}
	
	public void createStream() throws URISyntaxException, IOException {
		Stream ss = Stream.empty();
//		System.out.println(ss.count());
		
		Stream gey = Stream.of("hello","mike","testing");
		gey.forEach(System.out::println);
		System.out.println();
		
		String arr[] = {"vanakkam","tamizha"};
		Stream sss = Arrays.stream(arr);
		sss.forEach(System.out::println);
		System.out.println();
		
		Stream<String> st1 = Stream.<String>builder().add("hey").add("whats").add("up!").build();
		st1.forEach(System.out::println);
		System.out.println();
		
		Stream<Double> str = Stream.<Double>generate(()->new Generator().generated()).limit(5);
		str.forEach(System.out::println);
		System.out.println();
		
		IntStream.range(1, 5).forEach(System.out::print);
		System.out.println();
		
		"Hello world".chars().forEach((i)->{System.out.print((char)i);});
		System.out.println();
		
		Path path = Paths.get("abcdef.properties");
		Stream strem = Files.lines(path,Charset.forName("UTF-8"));
		strem.forEach(System.out::println);
		
	}
	
	public void checkPass() {
		Stream<Integer> strm = Stream.of(60,90,30,80,40);
		List<Integer> list = strm.filter(m->(m>=35)).map(s->(s+5)).sorted().collect(Collectors.toList());
		list.forEach(System.out::println);
		System.out.println();
		
		Stream<Integer> myStream = Stream.of(12,21,32,13,9);
		myStream.map(s->(increment(s))).forEach(System.out::println);
		
		Stream<String> sthream = Stream.of("abcd","dbca","1234","a1234");
		sthream.filter(n->check(n)).sorted().forEach(System.out::println);;
		
		
	}
	
	public void checkMinMax() {
		Stream<Integer> strm = Stream.of(2,3,1,10,9,7,6);
		int min = strm.min((i1,i2)->i1.compareTo(i2)).get();
		System.out.println("Min element is...."+min);
		
		Stream<Integer> rm = Stream.of(100,20,30,50,120,90);
		int max = rm.max((i1,i2)->i1.compareTo(i2)).get();
		System.out.println("Max element is...."+max);
		
		Integer[] i = (Integer[]) strm.toArray();
	}
	public int increment(int s) {
		return s+1;
	}
	
	public boolean check(String s) {
		if(s.contains("a")) {
			return true;
		}
		else {
			return false;
		}
	}
	
}

class Generator{
	public double generated() {
		return new Random().nextDouble();
	}
}