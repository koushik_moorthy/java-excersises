package lab.exe;

public class D2EX1 {
	public static void main(String[] args) {
		PBV pbv = new PBV();
		PBR1 pbr1 = new PBR1();
		PBR2 pbr2 = new PBR2();
		int val = 1000;
		System.out.println("Before Passing..."+val);
		pbv.changeVal(val);
		System.out.println("After Passing..."+val);
		System.out.println("Before Passing..."+pbr1.val);
		pbr2.changeVal(pbr1);
		System.out.println("After Passing..."+pbr1.val);
	}	
}
class PBV{
	public void changeVal(int val) {
		val = 0;
	}
}
class PBR1{
	int val=1000;
}
class PBR2{
	public void changeVal(PBR1 obj) {
		obj.val = 0;
	}
}