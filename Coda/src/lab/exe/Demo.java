package lab.exe;

public class Demo {
	public static void main(String[] args) {
		SingleTon ston = SingleTon.create();	
	}
}
	

class SingleTon{
	private SingleTon() {
		System.out.println("SingleTon Object Created");
	}
	static SingleTon st;
	public static SingleTon create() {
	if(st==null) {
		st= new SingleTon();
		return st;
	}
	return st;
	}
}