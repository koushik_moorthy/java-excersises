package serialization;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class SerializerDemo {
	public static void main(String[] args) throws Exception{
		Dog d1 = new Dog();
		d1.i = 30;
		d1.j = 40;
		WriteObject wr = WriteObject.writeObject(d1);
		d1.i = 40;
		d1.j = 100;
		d1 =  new ReadObject().readObject();
		System.out.println(d1.i+"    "+d1.j);
	}
}


class Dog implements Serializable{
	int i=10,j=20;
}


class WriteObject{
	public static WriteObject writeObject(Dog d) throws Exception{
		ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("log.dat"));
		oos.writeObject(d);
		return null;
	}
}

class ReadObject{
	public static Dog readObject() throws Exception{
		ObjectInputStream ois = new ObjectInputStream(new FileInputStream("log.dat"));
		Dog d1 = (Dog)ois.readObject();
		return (Dog)d1;
	}
}