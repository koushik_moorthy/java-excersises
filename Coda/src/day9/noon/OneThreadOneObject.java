package day9.noon;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class OneThreadOneObject {
	public static void main(String[] args) {
		new Thread(()->{
			
		}).start();
		
		ExecutorService es = Executors.newFixedThreadPool(2);
		
		es.execute(()->{
			Resource r = Factory.getResource();
			r.name= "First thread object";
			Resource r2 = Factory.getResource();
			System.out.println(r2.name);
			Resource r3 = Factory.getResource();
			System.out.println(r3.name);
			Factory.removeResourceFromthread();
			Resource r4 = Factory.getResource();
			System.out.println(r4.name);
		});
		
		
		es.execute(()->{
			Resource r= Factory.getResource();
			System.out.println("Second thread..."+r.name);
			Resource r2 = Factory.getResource();
			System.out.println("Second thread..."+r2.name);
		});
		
		es.shutdown();
	}
}


class Factory{
	private static ThreadLocal tlocal = new ThreadLocal();
	
	public static Resource getResource() {
		Resource r= (Resource)tlocal.get();
		if(r==null) {
			r = new Resource();
			tlocal.set(r);
			return r;
		}
		else {
			return r;
		}
	}
	
	public static void removeResourceFromthread() {
		if(tlocal.get()!=null) {
			tlocal.remove();
		}
		
	}
}


class Resource{
	String name="Koushik";
	static int i=0;
	public Resource() {
		System.out.println("Resource cons called"+i);
		i+=1;
	}
}