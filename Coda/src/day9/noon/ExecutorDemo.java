package day9.noon;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class ExecutorDemo {
	public static void main(String[] args) throws Exception {
		ExecutorService es = Executors.newFixedThreadPool(2);
		
		es.execute(()->{
			System.out.println("Calling child 1");
		});
		
		es.execute(()->{
			System.out.println("Calling child 2");
		});
		
		Future future = es.submit(()->{
			return "Hello World";
		});
		
		System.out.println("Future returned...."+future.get());
		
		es.shutdown();
	}
}


