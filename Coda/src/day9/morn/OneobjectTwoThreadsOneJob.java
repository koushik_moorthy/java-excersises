package day9.morn;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class OneobjectTwoThreadsOneJob {
	public static void main(String[] args) {
		Gun m416 = new Gun();
		
		ExecutorService es = Executors.newFixedThreadPool(2);
		
		es.execute(()->{
			for(int i=0;i<5;i++) {
				m416.fill();
				}
		});
		es.execute(()->{
			for(int i=0;i<5;i++) {
			m416.shoot();
			}
		});
		
		es.shutdown();
	}
	
}


class Gun{
	boolean flag;
	synchronized public void fill() {
		if(flag) {
			try{wait();}catch(Exception e) {}
		}
		System.out.println("Filling the gun");
		flag=true;
		notify();
	}
	
	synchronized public void shoot() {
		if(!flag) {
			try{wait();}catch(Exception e) {}
		}
		System.out.println("Shoottttt.......");
		flag=false;
		notify();
	}
}