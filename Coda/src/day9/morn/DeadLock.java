package day9.morn;

public class DeadLock {
	public static void main(String[] args) {
		DuckFrog df = new DuckFrog();
		new Thread(()->{
			for(int i=0;i<5;i++) {
				df.eatFrog();
			}
		}).start();
		
		new Thread(()->{
			for(int i=0;i<5;i++) {
				df.escapeDuck();
			}
		}).start();
	}
}


class DuckFrog{
	boolean flag;
	synchronized public void eatFrog() {
		if(flag) { 
			try {wait();}catch(Exception e) {}
		}
		System.out.println("Duck: Trying to eat Frog");
		flag= true;
		
	}
	synchronized public void escapeDuck() {
		if(!flag) { 
			try {wait();}catch(Exception e) {}
		} 
			System.out.println("Frog: Catch hold of duck's neck");
			flag= false;
	}
}