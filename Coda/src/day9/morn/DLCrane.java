package day9.morn;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class DLCrane {
	public static void main(String[] args) {
		Frog frog= new Frog();
		Crane crane= new Crane();
		
		ExecutorService es = Executors.newFixedThreadPool(2);
		
		es.execute(()->{
			crane.eat(frog);
		});
		
		es.execute(()->{
			frog.escape(crane);
		});
		
		es.shutdown();
		
	}
}

class Frog{
	synchronized public void escape(Crane c) {
		c.cranesLeavemethod();
	}
	
	synchronized public void frogLeavesMethod() {
		
	}
}

class Crane{
	synchronized public void eat(Frog f) {
		System.out.println("Eating .....");
		System.out.println("Eating .....");
		f.frogLeavesMethod();
		System.out.println("Over....");
	}
	
	synchronized public void cranesLeavemethod(){
		
	}
}