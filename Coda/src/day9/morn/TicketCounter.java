package day9.morn;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class TicketCounter {
	public static void main(String[] args) {
		ReservationCounter rc = new ReservationCounter();
		ExecutorService es = Executors.newFixedThreadPool(2);
		
		es.execute(()->{
			synchronized (rc) {
				Thread.currentThread().setName("dolu");
				rc.bookTicket(1000);
				rc.getChange();
				
			}	
			
		});
		
		es.execute(()->{
			synchronized (rc) {
				Thread.currentThread().setName("bolu");
				rc.bookTicket(500);
				rc.getChange();
				rc.drinkWater();
				
				
			}
		});
		
		es.shutdown();
	}
}

class ReservationCounter{
	int amt;
	
	public void bookTicket(int amt) {
		this.amt=amt;
		Thread t= Thread.currentThread();
		String name;
		name=t.getName();
//		try {Thread.sleep(10000);}catch(Exception e) {}
		System.out.println("Booked ticket given to "+name+" and the amt he gave is"+amt);
	}
	
	public void getChange() {
		int change = amt-100;
		Thread t = Thread.currentThread();
		String name = t.getName();
		System.out.println("Change given to "+name+" is "+change);
	}
	
	public void drinkWater() {
		System.out.println("Drinking water");
	}
}