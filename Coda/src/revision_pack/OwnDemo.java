package revision_pack;

public class OwnDemo {
	public static void main(String[] args) {
		Mom uma = new Mom();
		Son koushik = new Son();
		Item i = new Molaga();
		Item i1=new Maggi();
		uma.giveInstruction(koushik, i);
		uma.giveInstruction(koushik, i1);
	}
}


class Son{
	public void purchaseItems(Item i) {
		System.out.println(i);
	}
}

class Mom{
	public void giveInstruction(Son s,Item i) {
		s.purchaseItems(i);
	}
}

class MaligaiKadai{
	
}

abstract class Item{}

class Molaga extends Item{}
class Maggi extends Item{}
class Ragi extends Item{}