package revision_pack;

public class LambDemo {
	public static void main(String[] args) {
		//Anonymous Inner Class
		Phone p = new Phone() {
			@Override
			public void show() {
				// TODO Auto-generated method stub
				System.out.println("Calls SMS");
			}
		};
		p.show();
		
		//Lambda Expressions
		TV tv = ()-> System.out.println("TV is being operated");
		tv.operate();
		
	}
}

abstract class Phone{
	public abstract void show();
}



interface TV{
	public abstract void operate();
}