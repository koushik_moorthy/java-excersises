package revision_pack;

public class ProtoRev {
	public static void main(String[] args)throws Exception {
		Sheep mom = new Sheep();
		Sheep doll = mom.createProto();
		
		mom.name="I am mommy";
		doll.name="I am doll";
		
		System.out.println(mom.name+"......."+doll.name);
	}
}

class Sheep implements Cloneable{
	String name;
	public Sheep() {
		System.out.println("Cons called");
	}
	public Sheep createProto() throws Exception{
		try {
			return (Sheep)super.clone();
		}
		catch(Exception e) {
			return null;
		}
	}
}
