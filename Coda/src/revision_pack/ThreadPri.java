package revision_pack;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ThreadPri {
	public static void main(String[] args) {
		ExecutorService es = Executors.newFixedThreadPool(2);
		
		es.execute(()->{
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println("Child");
			Thread.currentThread().setName("MySecondThread");
			String str1 = Thread.currentThread().getName();
			System.out.println(str1);
//			Thread.currentThread().setPriority(2);
			
		});
		
		es.execute(()->{
			System.out.println("Parent");
			Thread.currentThread().setName("My First Thread");
//			Thread.currentThread().setPriority(10);
		});
		
		es.shutdown();
	}
}
