package revision_pack;

import java.lang.ref.SoftReference;
import java.lang.ref.WeakReference;

public class GarbColl {
	public static void main(String[] args) {
		Runtime r= Runtime.getRuntime();
		System.out.println("Before Grandpa birth.."+r.freeMemory());
		Grandpa somu= new Grandpa();
		System.out.println("After Grandpa birth.."+r.freeMemory());
		WeakReference<Grandpa> weak =new WeakReference<Grandpa>(somu);
//		SoftReference<Grandpa> soft =new SoftReference<Grandpa>(somu);
		somu = null;
		System.out.println("Grandpa is dead.."+r.freeMemory());
		r.gc();
		System.out.println("After Grandpa funeral...."+r.freeMemory());
		weak.get();
//		System.out.println("Grandpa age..."+somu.age);
		
	}
}


class Grandpa{
	String age;
	public  Grandpa() {
		for(int i=0;i<10000;i++) {
			this.age = new String(i + "");
		}
	}
	private String locationGold = "Under tree";
	private void getLocation() {
		System.out.println("Location is "+locationGold);
	}
	public void finalize() {
		getLocation();
		System.out.println("Finally Called...");
	}
}